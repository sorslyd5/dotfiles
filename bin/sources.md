- regex
    - https://regex101.com/

- path variable
    - https://stackoverflow.com/questions/20054538/add-a-bash-script-to-path

- bash stuff
    - https://linuxhandbook.com/bash-arguments/
    - https://linuxhandbook.com/bash-variables/
    - https://linuxhandbook.com/if-else-bash/

- check file type
    - https://stackoverflow.com/questions/407184/how-to-check-the-extension-of-a-filename-in-a-bash-script
    - https://stackoverflow.com/questions/374930/validating-file-types-by-regular-expression

- passing a wildcard operand to bash command requires '' to evaluate, otherwise bash script will only take first item in evaluation
    -`  command.sh *.pdf     `
    -`  command.sh '*.pdf'   `
