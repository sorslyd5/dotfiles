" checks if media is wrapped/unwrapped, then toggles
" wraps media: for deploying jekyll site such that links aren't broken
" unwraps media: for local preview

" https://learnxinyminutes.com/docs/vimscript/
" https://askubuntu.com/questions/167971/how-do-i-run-a-script-from-within-the-vim-editor
" https://devhints.io/vimscript#functions
" https://dev.to/dlains/create-your-own-vim-commands-415b
" https://vi.stackexchange.com/questions/13187/pass-variable-in-vimscript-function-to-split
" https://stackoverflow.com/questions/32368309/how-to-setup-vimscript-command-to-call-a-function

function! JekWrap_f(state)
    if a:state == "local"         "if local mode
        %s/]({{ site.url }}{{ site.baseurl }}\//](\//g
    elseif a:state == "global"    "if live mode
        %s/](\//]({{ site.url }}{{ site.baseurl }}\//g
    else
        echo "invalid arg"
    endif
endfunction

command -nargs=1 Jekwrap call JekWrap_f(<q-args>)
